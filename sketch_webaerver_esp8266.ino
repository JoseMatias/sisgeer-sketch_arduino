#include "EmonLib.h"
#include <SoftwareSerial.h>

/* RX pino 2, TX pino 3 */
SoftwareSerial esp8266(2, 3);

/* Monitor de Energia */
EnergyMonitor energyMonitor;

/* Variaveis estaticas */
#define PSENSOR A0
#define TENSAO_REDE 220
#define DEBUG true
#define SSID  LabCCOMP
#define PASSWORD #labccomp#
#define HOST "10.87.41.207"
#define MINIMUM_MEASURE 0.10

double irms = 0.0;
double power = 0.0;
double kwh = 0.0;
double kwh_ = 0.0;
unsigned long ltmillis, tmillis, timems, sendTimer;

void setup()
{
  Serial.begin(9600);   /* Serial Monitor - debug */
  esp8266.begin(19200); /* Serial comunicação - ESP8266 */

  Serial.println("***** INICIAR CONFIGURACAO - ESP8266 *****\r\n");

  /* Testar o modulo. */
  Serial.println("TESTA O FUNCIONAMENTO DO MODULO");
  sendData("AT\r\n",3000, DEBUG);

  /* Resetar modulo WiFi (ESP8266). */
  //Serial.println("Resetar modulo wifi (ESP8266).");
  //sendData("AT+RST\r\n", 3000, DEBUG);

  /* Exibir versão do firmware do módulo ESP8266. */
  Serial.println("EXIBE A VERSAO DO FIRMWARE DO ESP12");
  sendData("AT+GMR\r\n", 3000, DEBUG);

  /* Configura módulo como estação. */
  Serial.println("CONFIGURA O MODULO COMO ESTACAO");
  sendData("AT+CWMODE=1\r\n", 3000, DEBUG);

  /* Lista redes visiveis. */
  //Serial.println("Listando redes visiveis");
  //sendData("AT+CWLAP\r\n", 10000, DEBUG); 

  /* Conectar a rede WiFi (Via Roteador). */
  Serial.println("CONECTA A REDE WIFI (MODEM)");
  sendData("AT+CWJAP=\"LabCCOMP\",\"#labccomp#\"\r\n", 15000, DEBUG);

  /* Exibe o endereco IP da conexão. */
  //Serial.println("Endereco IP da conexao");
  //sendData("AT+CIFSR\r\n", 3000, DEBUG);

  /* Configura para uma unica conexão. */
  Serial.println("CONFIGURA O MODULO PARA CONEXAO UNICA");
  sendData("AT+CIPMUX=0\r\n", 3000, DEBUG);

  /* Inicia um conexão para com um endereço remoto, Porta 80. */
  //Serial.println("Inicia um conexao para com um endereco remoto, Porta 80.");
  //sendData("AT+CIPSTART=4,\"TCP\",\"192.168.56.1\",80\r\n", 5000, DEBUG);

  /* Configurar a porta analogica do sensor. */
  /* Calibração -  (N° de voltas da bobina)/(Resistencia de carga) -> (2000/33) = 60,606 ~ 60.6 */
  energyMonitor.current(PSENSOR, 60);

  sendTimer = millis();

  Serial.println("***** CONFIGURACAO CONCLUIDA *****\r\n");
}
 
void loop()
{
  /* Calcula o tempo total referente a medida do consumo de potencia.  */
  ltmillis = tmillis;
  tmillis = millis();
  timems = tmillis - ltmillis;
  
  /* Calcula a corrente e a potencia aparente.  */
  irms = energyMonitor.calcIrms(1480);

  if (irms > MINIMUM_MEASURE) {
    /* Calcula o total da potencia consumida em Kwh. */
    kwh = (((irms * TENSAO_REDE)/1000.0) * (1.0/3600.0) * (timems/1000.0));
    kwh_ += kwh; 
  } else {
    irms = 0.0;
  }
  
  String str_cur = String(irms, 5);
  String str_pow = String(irms*TENSAO_REDE, 5);
  String str_kwh = String(kwh, 5);
  String str_kwh_ = String(kwh_, 5);

  delay(500);
  
  /* Mostra o valor da corrente (Irms). */
  Serial.print("Corrente (A): ");
  Serial.print(str_cur);
  Serial.print(" | Potencia (W): ");
  Serial.print(str_pow);
  Serial.print(" | Consumo (Kwh): ");
  Serial.println(str_kwh_);

  if (irms > MINIMUM_MEASURE) {
    openCommunicationTCP();
    sendDataTcpRequest(str_cur, str_pow, str_kwh);
  }

  delay(5000);
}

void sendData(String command, const int timeout, boolean debug)
{
  String response = "";
  esp8266.print(command);
  long int timeResponse = millis();

  if (debug) {
    while ((timeResponse + timeout) > millis()) {
      while (esp8266.available()) {
        char c = esp8266.read();
        response += c;
      }
    }

    Serial.println(response);
  } else {
    serialFlush();
  }
}

void openCommunicationTCP()
{
  sendData("AT+CIPSTART=\"TCP\",\"104.131.165.12\",80\r\n\0", 5000, DEBUG);
}

void sendDataTcpRequest(String cor, String pot, String kwh)
{
  char data[120], preSend[20];
  char tmpCor[20], tmpPot[20], tmpKwh[20];

  cor.toCharArray(tmpCor, cor.length()+1);
  pot.toCharArray(tmpPot, pot.length()+1);
  kwh.toCharArray(tmpKwh, kwh.length()+1);

  sprintf(data, "GET /sisgeer/energyReading.php?cor=%s&pot=%s&kwh=%s HTTP/1.1\r\nHost: 104.131.165.12\r\n\r\n", tmpCor, tmpPot, tmpKwh);
  sprintf(preSend, "AT+CIPSEND=%d\r\n\0", strlen(data));

  sendData(preSend, 2000, DEBUG);
  sendData(data, 5000, DEBUG);
}

void serialFlush()
{
  while(esp8266.available() > 0) {
    char t = esp8266.read();
  }
}

